#!/usr/bin/env python3
import sys
import argparse
import math
from DSU import *
import numpy as np
# import matplotlib as mpl
# import matplotlib.pyplot as plt
import random
import gmpy

capmax = 1000
capmin = 1
def noCapacityGraphgen(n, tp):
    if tp in ["cu","cu"]:
        G = np.ones((n,n),dtype=int)
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp in ["r0.5u","r0.5r"]:
        G = np.random.randint(0,2,size=(n,n))
        for i in range(0,n):
            for j in range(i,n): 
                cap = random.randint(capmin,capmax)
            G[i,j] = cap*G[i,j]
        return G
    elif tp in ["mu","mr"]:
        if not gmpy.is_square(n):
            sys.exit("# of vertices must be square number.")
        sq = math.sqrt(n)
        G = np.zeros((n,n),dtype=int)
        for i in range(0,n):
            if i + sq < n:
                G[i,i+sq] = 1
            if (i+1)%sq:
                G[i,i+1] = 1
        return G
    else:
        sys.exit("type "+tp+" is not supported in this program.")
    
    G = np.random.randint(1,1000,size=(n,n))
    return G

def graphgen(n, tp):
    if tp=="cu":
        cap = random.randint(capmin,capmax)
        G = np.ones((n,n),dtype=int)
        G = cap*G
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp == "cr":
        G = np.random.randint(capmin,capmax+1,size=(n,n))
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp == "r0.5u":
        cap = random.randint(capmin,capmax)
        G = np.random.randint(0,2,size=(n,n))
        G = cap*G
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp =="r0.5r":
        G = np.random.randint(0,2,size=(n,n))
        for i in range(0,n):
            for j in range(i,n): 
                cap = random.randint(capmin,capmax)
                G[i,j] = cap*G[i,j]
        return G
    elif tp=="mu" or tp == "mr":
        if not gmpy.is_square(n):
            sys.exit("# of vertices must be square number.")
        sq = math.sqrt(n)
        if tp=="mu":
            cap = random.randint(capmin,capmax)
            G = np.zeros((n,n),dtype=int)
            for i in range(0,n):
                if i + sq < n:
                    G[i,i+sq] = cap
                if (i+1)%sq:
                    G[i,i+1] = cap
            return G
        else:
            G = np.zeros((n,n),dtype=int)
            for i in range(0,n):
                if i + sq < n:
                    G[i,i+sq] = random.randint(capmin,capmax)
                if (i+1)%sq:
                    G[i,i+1] = random.randint(capmin,capmax)
            return G
    else:
        sys.exit("type "+tp+" is not supported in this program.")

    G = np.random.randint(1,1000,size=(n,n))
    return G

def subsetgen(n,v,mins,maxs,minbw,maxbw):
    req = []
    bw = []
    s = []
    vertices = list(range(1,v+1))
    for i in range(0,n):
        bw.append(random.randint(minbw,maxbw))
    for i in range(0,n):
        s.append(random.randint(mins,maxs))
    for i in range(0,n):
        req.append((sorted(random.sample(vertices,s[i])),bw[i]))
    return req

def printdata(g,req, n):
    edgeCnt = 0
    for i in range(0,n):
        for j in range(i,n):
            if g[i,j]:
                edgeCnt+=1
    print(n, edgeCnt)
    for i in range(0,n):
        for j in range(i,n):
            if g[i,j]:
                print(i+1, j+1, g[i,j])
    print(len(req))
    for i in range(0,len(req)):
        bw = req[i][1]
        v = req[i][0]
        print(len(v))
        for j in range(0,len(v)):
            print(v[j],end=" ")            
        print(bw)
    return

def embedRequirement(g,req,v):
    dsu = DSU(v)
    cnt = 0
    used = defaultdict(lambda: defaultdict(bool))
    degree = [0]*v
    while not dsu.contain(req[0]):
        for i in range(0,v):
            for j in range(i,v):
                if (not g[i,j]) or j in used[i]:
                    continue
                if random.randint(0,2) and dsu.find(i)!=dsu.find(j):# connecting i and j does not create cycle?
                    used[i][j] = True
                    used[j][i] = True
                    degree[i] += 1
                    degree[j] += 1
                    dsu.union(i,j)
    #pruning
    for i in range(0,v):
        if i in req[0]:
            continue
        if degree[i]==1:
            j = list(used[i].keys())[0]
            used[min(i,j)][max(i,j)] = False
            degree[i]=0
            degree[j]-=1

    return used

def embedRequirements(g,req,v):
    for r in req:
        used = embedRequirement(g,r,v)
        # print(used)
        for key1, value1 in used.items():
            for key2, value2 in value1.items():
                if g[key1,key2]:
                    if g[key1,key2] == 1:
                        g[key1,key2] = r[1]
                    else:
                        g[key1,key2] += r[1]
    for i in range(0,v):
        for j in range(i,v):
            if g[i,j]:
                g[i,j] += 5 # add a little bit of capacity to every edge

def subtractWeight(g,v):
    for i in range(0,v):
        for j in range(i,v):
            # g[i,j] -= random.randint(0,g[i,j]//2);
            g[i,j] -= random.randint(0,10);

parser = argparse.ArgumentParser()
parser.add_argument("-v",required=True,help="Number of verteces to be generated",type=int)
parser.add_argument("-t",required=True,help="Number of subsets to be generated",type=int)
parser.add_argument("--type",required=True,help="type of graph[cu|cr|r0.5u|r0.5|mu|mr]",type=str)
parser.add_argument("--solvable",default=False,help="If this flag is set, the output becomes solvable. If not set, the ouput might be solvable, but in most cases not solvable", type=bool)

args = parser.parse_args()

# Number of vertices to be generated
v = args.v
# Number of steiner set to be generated
T = args.t

graphType = args.type

g = noCapacityGraphgen(v,graphType)

maxbw = capmax//T
minbw = 1
maxs = v
mins= 2

req = subsetgen(T,v,mins,maxs,minbw,maxbw) 
embedRequirements(g,req,v)

if not args.solvable:
    subtractWeight(g,v)

printdata(g,req,v)
