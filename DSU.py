import math
from collections import defaultdict

class DSU(object):
    def __init__(self, n):
        self._parent = list(range(n))
        self._rank = [0] * n

    def find(self, i):
        if self._parent[i] == i:
            return i
        else:
            self._parent[i] = self.find(self._parent[i])
            return self._parent[i]

    def union(self, i, j):
        root_i = self.find(i)
        root_j = self.find(j)
        if root_i != root_j:
            if self._rank[root_i] < self._rank[root_j]:
                self._parent[root_i] = root_j
            elif self._rank[root_i] > self._rank[root_j]:
                self._parent[root_j] = root_i
            else:
                self._parent[root_i] = root_j
                self._rank[root_j] += 1
    def contain(self,nodes):
        if len(nodes)==0:
            return True
        id = self.find(nodes[0]-1) 
        for i in range(1,len(nodes)):
            if self.find(nodes[i]-1)!=id:
                return False
        return True

